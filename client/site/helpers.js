/**
 * Global Client JavaScript (Available in all templates)
 */

Handlebars.registerHelper('publicSettings', function () {
    return Meteor.settings.public;
});
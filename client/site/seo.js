/*
 * SEO Configuration for default values.
 * Used only if nothing else is specified in the route hooks or SEO collection
 *
 * See Also: https://atmospherejs.com/manuelschoebel/ms-seo
 *
 * Note: it is important that the 'spiderable' package is installed for SEO to work correctly
 *
 */

Meteor.startup(function () {
    var title = Meteor.settings.public.meta.title;
    var description = Meteor.settings.public.meta.description;
    var author = Meteor.settings.public.meta.author;
    var avatar = Meteor.settings.public.meta.avatar;
    return SEO.config({
        title: title,
        rel_author: Meteor.settings.public.meta.google_author,
        meta: {
            'author'     : author,
            'description': description
        },
        og: {
            'image': avatar,
            'type':  'website',
            'title': title
        },
        twitter: {
            'title': title,
            'description': description
        }
    });
});
# Meteor Project Foundation

## Synopsis
There are several boilerplate's and tools to initialise a Meteor project. However, they
don't always provide a lot of documentation on how they are put together, why various
decisions were made and so on.

This Meteor foundation is designed to get developers up and running with the most commonly
used packages and conventions, while explaining the why and how.

## Getting Started
See [A running example](http://foundation.meteor.com) for further information

## Packages Used

```
meteor remove insecure
meteor remove autopublish
meteor add iron:router
meteor add manuelschoebel:ms-seo
meteor add aldeed:simple-schema
meteor add aldeed:collection2
meteor add aldeed:autoform
meteor add naxio:flash
meteor add less
meteor add nemo64:bootstrap
meteor add fortawesome:fontawesome
meteor add wizonesolutions:underscore-string
meteor add zimme:iron-router-active
meteor add spiderable
meteor add msavin:mongol
```


## Development Tools

Enable 'Mongol' (```msavin:mongol```) with the ```CTRL +M``` keyboard shortcut to get access to collections and subscriptions 
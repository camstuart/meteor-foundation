Router.configure({
    layoutTemplate:     'layout',
    notFoundTemplate:   'page-not-found',
    loadingTemplate:    'page-loading'
});

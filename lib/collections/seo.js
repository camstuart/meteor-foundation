/*
 * SEO Configuration, dynamic with iron router.
 *
 * See Also: https://atmospherejs.com/manuelschoebel/ms-seo
 *
 */


//SeoCollection.insert({
//    "route_name" : "home", // the name of the Iron-Router route
//    "title" : "Title of your home site",
//    "meta" : [
//        {"description": "This is the description of the document"},
//        // add more meta tags
//    ],
//    "og" : [
//        { "image": "http://your-domain.com/images/image.jpg" },
//        // add more open graph tags
//    ]
//});